package com;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class JsonReader {

    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
        URL is = new URL(url);
        HttpURLConnection httpcon = (HttpURLConnection) is.openConnection();
        httpcon.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.93 Safari/537.36");
        BufferedReader rd = null;
        try {
            rd = new BufferedReader(new InputStreamReader(httpcon.getInputStream(), Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json = new JSONObject(jsonText);
            return json;
        } finally {
            rd.close();
        }

    }

    public static void main(String[] args) throws IOException {
        Integer int_age = Integer.parseInt(args[0]);
        String str_District = args[1];
        System.out.println("<b>List of vaccination centers for the age "+int_age+"plus:</b>");
        publishCenters(str_District,int_age);

    }

    public static void publishCenters(String strDistId, Integer intAge) throws IOException, JSONException{

        int int_min_age_limit, int_available_capacity, int_pincode,int_Dose1, int_Dose2;
        String str_name,str_address,str_fee_type,str_date,str_url_date,str_district_name,stc_Vaccine;

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        LocalDateTime now = LocalDateTime.now();
        str_url_date = dtf.format(now);

        JSONObject json = readJsonFromUrl("https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id="+strDistId+"&date="+str_url_date);

        // System.out.println(json.toString());

        JSONArray jsonCentersArray = json.getJSONArray("centers");

        JSONArray jsonSessionsArray = null;

        //    JSONArray jsonSessionsArray = jsonCentersArray.getJSONObject(0).getJSONArray("sessions");

        //    System.out.println(jsonCentersArray.length());

        int counter =0;
        for(int i=0;i<jsonCentersArray.length();i++){

            str_name = jsonCentersArray.getJSONObject(i).getString("name");
            str_address = jsonCentersArray.getJSONObject(i).getString("address");
            str_fee_type = jsonCentersArray.getJSONObject(i).getString("fee_type");
            int_pincode = jsonCentersArray.getJSONObject(i).getInt("pincode");
            str_district_name = jsonCentersArray.getJSONObject(i).getString("district_name");

            jsonSessionsArray = jsonCentersArray.getJSONObject(i).getJSONArray("sessions");
            Boolean bFirstFlag = true;

            for(int j=0;j<jsonSessionsArray.length();j++) {
                int_available_capacity = jsonSessionsArray.getJSONObject(j).getInt("available_capacity");
                int_Dose1 = jsonSessionsArray.getJSONObject(j).getInt("available_capacity_dose1");
                int_Dose2 = jsonSessionsArray.getJSONObject(j).getInt("available_capacity_dose2");
                int_min_age_limit = jsonSessionsArray.getJSONObject(j).getInt("min_age_limit") ;
                stc_Vaccine = jsonSessionsArray.getJSONObject(j).getString("vaccine") ;

                str_date = jsonSessionsArray.getJSONObject(j).getString("date");

                if(int_available_capacity > 0 && int_min_age_limit==intAge )
                {
                    if(bFirstFlag){
                        bFirstFlag = false;
                        counter = counter +1;
                        System.out.println("");
                        System.out.println(counter+")"+str_name+ ","+ int_pincode);
                    }

                    if(strDistId.contentEquals("294") || strDistId.contentEquals("265") || strDistId.contentEquals("276")) {
                        //  System.out.println(strDistId);
                        break;
                    }
                    else{
                        if(int_Dose1==0) {
                            System.out.println(" <b>Date:</b>"+str_date+" <b>Vaccine:</b>"+stc_Vaccine+" <b>D2:</b>"+int_Dose2);
                        }
                        else if(int_Dose2==0) {
                            System.out.println(" <b>Date:</b>"+str_date+" <b>Vaccine:</b>"+stc_Vaccine+" <b>D1:</b>"+int_Dose1);
                        }
                        else{
                            System.out.println(" <b>Date:</b>"+str_date+" <b>Vaccine:</b>"+stc_Vaccine+" <b>D1:</b>"+int_Dose1+ " <b>D2:</b>"+int_Dose2);
                        }
                    }

                }
            }
        }
        System.out.println("\n<strong>SignIn for Vaccination</strong> "+"<a href='https://selfregistration.cowin.gov.in/'>https://selfregistration.cowin.gov.in/</a>");
    }
}